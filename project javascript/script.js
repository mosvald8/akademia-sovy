var fname;
var lname;
var birthdate;
var gender;

function overenie() {
    fname = document.getElementById("fname").value;
	  fname = fname.trim();
    lname = document.getElementById("lname").value;
    lname = lname.trim();

    birthdate = document.getElementById("birthdate").value;
    var dnes = new Date();
    var datumNarodenia = new Date(birthdate);

    if (fname.length<2){
        chyba(1);
    }
    else if (lname.length<2){
        chyba(2);
    }
    else if (dnes<datumNarodenia){
        chyba(3);
    }
    else {
         chyba(0);
    }
}

function chyba(vstup){
    switch(vstup){
        case 1:
            document.getElementById("errfname").innerHTML = "Meno musi mat minimalne 2 znaky.";
            break;
        case 2:
            document.getElementById("errlname").innerHTML = "Priezvisko musi mat minimalne 2 znaky.";
            break;
        case 3:
            document.getElementById("errdate").innerHTML = "Zly datum.";
            break;
        case 0:
            pridanie();
    }
}

function pridanie() {
    var osoba = {};
    osoba["fname"]=fname;
    osoba["lname"]=lname;
    osoba["birthdate"]=birthdate;
    osoba["gender"]=gender;
    $('#zoznam tr:last').after('<tr><td>'+osoba.fname+'</td><td>'+osoba.lname+'</td><td>'+osoba.birthdate+'</td></tr>');
}

$(document).ready(function(){
    $("#fname").keyup(function(event) {
        var meno=$("#fname").val();
        if(meno.trim().length>1){
            $("#errfname").html("");
        }
    });

    $("#lname").keyup(function(event) {
        var priezvisko=$("#lname").val();
        if(priezvisko.trim().length>1){
            $("#errlname").html("");
        }
    });

    $("#birthdate").change(function(event) {
        var datumNarodenia = new Date($("#birthdate").val());
        var dnes = new Date();
        if(dnes>=datumNarodenia){
            $("#errdate").html("");
        }
    });
})
